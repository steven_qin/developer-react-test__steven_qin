import React from 'react'
import styled from 'styled-components'
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'
import {Navbar} from 'react-bootstrap'
import {withRouter} from 'react-router-dom'
import Color from 'color'

const StyledNavbar = styled(Navbar)`
  min-height:64px;
  background:${props => props.theme.colors.dark};
  color:white;
  margin-bottom:0;
  border:none;
  border-radius:0;
  .navbar-header{
    .navbar-brand{
      // text-transform:uppercase;
      color:${props => props.theme.colors.white};
      background-image:url(${props => props.theme.header.logo.url});
      background-size:${props => props.theme.header.logo.width}px ${props => props.theme.header.logo.height}px;
      height:${props => props.theme.header.logo.height}px;
      background-repeat:no-repeat;
      margin-top:${props => (props.theme.header.height - props.theme.header.logo.height) / 2}px;
      display: flex;
      align-items: center;
      .brand-name{
        margin-left:${props => props.theme.header.logo.width}px;
        font-weight:${props => props.theme.fontWeightBold};
        font-size:20px;
      }
      .brand-section{
        font-weight:${props => props.theme.fontWeight};
        font-size:18px;
      }
      &.has-app-icon{
        background-image:none;
        img{
          height:${props => props.theme.header.logo.height}px;
        }
        .brand-name{
          margin-left:10px;
        }
      }
    }
    .navbar-toggle{
      border:none;
      border-radius:0;
      background-color:${props =>  Color(props.theme.colors.dark).lighten(0.1).saturate(0.5).hex()};
      &.collapsed{
        background-color:${props => props.theme.colors.primary};
        &:hover, &:active{
          background-color:${props => props.theme.colors.white};
          .icon-bar{
            background-color:${props => props.theme.colors.primary};
          }
        }
      }
      .icon-bar{
        background-color:${props => props.theme.colors.white};
        width: 17px;
        height: 3px;
      }
      &:hover, &:active{
        background-color:${props => props.theme.colors.white};
        .icon-bar{
          background-color:${props => Color(props.theme.colors.dark).lighten(0.1).saturate(0.5).hex()};
        }
      }
    }
  }
  .navbar-nav{
    margin: 0 -15px;
    @media (min-width: 768px) {
      margin: 0;
    }
  }
  .navbar-collapse{
    border:none;
  }
`
const Header = ({dispatch, location})=>{
  let brandLink = '/'
  let brandSection = false
  let brandClass = 'btn-home'
  return (
    <StyledNavbar className="header navbar-fixed-top" fixed="top">
      <Navbar.Header>
        <Navbar.Brand>
          <Link to={brandLink} className={brandClass}>
            <span className="brand-name main-title">{'Cogniss'}</span>
            {brandSection && <span className="brand-section">&nbsp;| {brandSection}</span>}
          </Link>
        </Navbar.Brand>
      </Navbar.Header>
    </StyledNavbar>)
}
function mapDispatchToProps(dispatch) {
  return {dispatch: dispatch}
}
export default withRouter(connect(null, mapDispatchToProps)(Header))
