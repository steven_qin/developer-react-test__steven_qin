import {useState} from 'react'
import { createContainer } from "unstated-next"

function useConfig({config: initConfig}) {
  let [config, setConfig] = useState(initConfig)
  return { config, setConfig }
}

export default createContainer(useConfig)
