import React, {useState} from 'react';
import {Modal} from "react-bootstrap";
import {Button} from "react-bootstrap";
import {Link} from "react-router-dom";
import Intent from './intent';
import ScrollArea from 'react-scrollbar';
import Tabs from './termsTab';


function MyVerticallyCenteredModal(props) {
    const [checked, setChecked] = useState(false);

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">
                    Terms & Privacy Policy
                </Modal.Title>
            </Modal.Header>
            <ScrollArea
                speed={0.8}
                className="area"
                contentClassName="content"
                horizontal={false}
                style={{height: "30rem", margin: "1rem", padding: "1rem"}}
            >
                <Intent/>
                <Modal.Body>
                    <Tabs/>
                </Modal.Body>
            </ScrollArea>
            <Modal.Footer>
                <form>
                    <input type="checkbox" defaultChecked={checked}
                           onChange={() => setChecked(!checked)}/>
                    <label htmlFor="checkbox"><p>I have read and agree to the Terms and Privacy Policy</p></label>
                </form>
                <Button variant="outline-primary" onClick={props.onHide}
                        style={{marginRight: "1rem", borderRadius: "23px"}}>Cancel
                </Button>
                {checked ?
                    <Link to="register"><Button variant="primary" style={{
                        marginLeft: "1rem",
                        borderRadius: "23px"
                    }}>Accept</Button></Link> :
                    <Button variant="primary" style={{marginLeft: "1rem", borderRadius: "23px"}} disabled>Accept</Button>}
            </Modal.Footer>
        </Modal>
    );
}

const Popup = () => {
    const [modalShow, setModalShow] = React.useState(false);

    return (
        <>
            <Link onClick={() => setModalShow(true)} className="btn-registration">Create an account?</Link>
            <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </>
    );
};

export default Popup;