import React, {useState} from 'react';
import {Alert, Collapse} from "react-bootstrap";
// import {Button} from "react-bootstrap";

const Intent = () => {
    const [open, setOpen] = useState(false);

    return (
        <>
            <Alert variant="secondary" style={{textAlign: "left", color: "grey", margin: "1.5rem", padding:"1rem", borderRadius:"1rem", backgroundColor: "#F1F1F1"}}>
                <div
                    onClick={() => setOpen(!open)}
                    aria-controls="example-collapse-text"
                    aria-expanded={open}
                    style={{display: "flex"}}
                >
                    <div style={{width: "95%"}}><h5>Statement of intent</h5></div>
                    <div style={{width: "3%"}}><h4>^</h4></div>
                </div>


                <Collapse in={open}>
                    <div id="example-collapse-text" style={{textAlign: "left"}}>
                        We want you to know exactly how our services work and why we need your details. Reviewing our
                        policy will help you continuing using the app with peace of mind.
                    </div>
                </Collapse>
            </Alert>
        </>
    );
};

export default Intent;