import styled from 'styled-components'
export default styled.button `
  background:none;
  border:none;
  color:${props => props.disabled?props.theme.colors.lightGrey :props.theme.colors.primary};
  &:hover, &:focus{
    color:${props => props.disabled?props.theme.colors.lightGrey :props.theme.colors.dark};
    text-decoration: underline;
  }
`
