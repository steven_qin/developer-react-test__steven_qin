import React from 'react';
import { LinkContainer } from 'react-router-bootstrap'
import {Breadcrumb} from 'react-bootstrap'
import _ from 'lodash'
export default ({links})=>{
  return (<Breadcrumb>
    {_.map(links, link =>{
      const {to, label} = link
      return (<LinkContainer key={to} to={to}>
        <Breadcrumb.Item>{label}</Breadcrumb.Item>
      </LinkContainer>)
    })}
  </Breadcrumb>)
}
